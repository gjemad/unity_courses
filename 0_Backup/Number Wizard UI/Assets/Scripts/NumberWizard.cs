﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NumberWizard : MonoBehaviour {

	int max;
	int min;
	int guess;

    public int maxGuessesAllowed = 5;

    public Text GuessText;

	void Start ()
    {
		StartGame ();
	}
		
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.UpArrow)) {
            GuessHigher();
		} else if (Input.GetKeyDown(KeyCode.DownArrow)) {
            GuessLower();
		}
	}

	void StartGame ()
    {
		max = 1000;
		min = 1;
		guess = Random.Range(min, max);
        NextGuess();
	}

    public void GuessHigher()
    {
        min = guess;
        NextGuess();
    }

    public void GuessLower()
    {
        max = guess;
        NextGuess();
    }

    void NextGuess ()
    {
		guess = Random.Range(min,max+1);
        GuessText.text = guess.ToString();
        maxGuessesAllowed = maxGuessesAllowed - 1;
        if (maxGuessesAllowed <= 0)
        {
            SceneManager.LoadScene("Win");
        }
	}
}