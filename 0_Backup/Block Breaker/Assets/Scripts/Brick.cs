﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brick : MonoBehaviour {

    public Sprite[] hitSprites;
    public AudioClip breakSound;
    public static int breakableCount = 0;
    public GameObject peSmoke;
    public GameObject smokePuffColor;

    private int timesHit;
    private LevelManager levelManager;
    private bool isBreakable;


    // Use this for initialization
    void Start () {
        isBreakable = (this.tag == "Breakable");
        // Teller hvor mange Breakable blokker som eksisterer
        if (isBreakable)
        {
            breakableCount++;
        }
        timesHit = 0;
        levelManager = GameObject.FindObjectOfType<LevelManager>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(breakSound, transform.position);
        if (isBreakable)
        {
            HandleHits();
        }
    }

    void HandleHits()
    {
        timesHit++;
        // Setter maxHits til antall Sprites som finnes for objekt (gjør det lettere å holde styr på i Unity)
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            breakableCount--;
            levelManager.BrickDestroyed();
            levelManager.UpdateBrickCount();
            PuffSmoke();
            Destroy(gameObject);
        }
        else
        {
            LoadSprites();
        }
    }

    void PuffSmoke ()
    {
        GameObject smokePuff = Instantiate(peSmoke, gameObject.transform.position, Quaternion.identity);
        ParticleSystem.MainModule main = smokePuff.GetComponent<ParticleSystem>().main;
        main.startColor = gameObject.GetComponent<SpriteRenderer>().color;
    }

    void LoadSprites()
    {
        int spriteIndex = timesHit - 1;
        this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
    }
}
