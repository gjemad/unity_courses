﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    public bool autoPlay = false;


    private Ball ball;

    void Start()
    {
        // Her opprettes det et 3D objekt for Paddle hvor vi da låser Y posisjonen til hvor vi har plassert objektet i scenen
        ball = GameObject.FindObjectOfType<Ball>();
    }

	void Update () {
        if (!autoPlay)
        {
            PlayWithMouse();
        }
        else
        {
            AutoPlay();
        }
	}

    void PlayWithMouse()
    {
        Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
        // Kalkulerer X-posisjonen til musen og deler det på bredden på skjermoppløsning og ganger det igjen med 16 (World Units, Google it!)
        float mousePosInBlock = (Input.mousePosition.x / Screen.width * 16);
        // Setter paddlePos sin X-posisjon til å gå til minimum 0.5f og maksimum 15.5f
        paddlePos.x = (Mathf.Clamp(mousePosInBlock, 0.5f, 15.5f));
        this.transform.position = paddlePos;
    }

    void AutoPlay()
    {
        Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
        float ballPos = ball.transform.position.x;
        paddlePos.x = (Mathf.Clamp(ballPos, 0.5f, 15.5f));
        this.transform.position = paddlePos;
    }
}