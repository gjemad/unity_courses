﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public Text brickCount;

    public void LoadLevel(string name)
    {
        Brick.breakableCount = 0;
        SceneManager.LoadScene(name);
        //Debug.Log("Level \"" + name + "\" loaded successfully!");
    }
    public void QuitGame()
    {
        //Debug.Log("Quit button clicked!");
        Application.Quit();
    }
    public void LoadNextLevel()
    {
        Brick.breakableCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void BrickDestroyed ()
    {
        if (Brick.breakableCount <= 0)
        {
            LoadNextLevel();
        }
    }
    public void UpdateBrickCount()
    {
        brickCount.text = ("Bricks: " + Brick.breakableCount);
    }
}
