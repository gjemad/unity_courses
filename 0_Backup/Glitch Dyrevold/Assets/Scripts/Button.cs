﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour {

    public GameObject prefabToSpawn;
    public static GameObject selectedDefender;

    private Button[] buttonArray;
    private Text costText;

	// Use this for initialization
	void Start () {
        buttonArray = FindObjectsOfType<Button>();
        costText = GetComponentInChildren<Text>();

        GetComponent<SpriteRenderer>().color = Color.black;

        foreach (Button button in buttonArray)
        {
            costText.text = prefabToSpawn.GetComponent<Defender>().starCost.ToString();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        foreach (Button thisButton in buttonArray)
        {
            thisButton.GetComponent<SpriteRenderer>().color = Color.black;
        }

        GetComponent<SpriteRenderer>().color = Color.white;
        selectedDefender = prefabToSpawn;
    }
}
