﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIO : MonoBehaviour {

    public float FadeInTimeInSeconds;

    private Image fadePanel;
    private Color currentColor = Color.black;

	// Use this for initialization
	void Start () {
        fadePanel = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad < FadeInTimeInSeconds)
        {
            float alphaChange = Time.deltaTime / FadeInTimeInSeconds;
            currentColor.a -= alphaChange;
            fadePanel.color = currentColor;
        } else
        {
            gameObject.SetActive(false);
        }
	}
}
