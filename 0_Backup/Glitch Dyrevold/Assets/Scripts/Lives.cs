﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour {

    public int lives;
    public enum Status { SUCCESS, FAILURE }

    private Text lifeText;

    void Start () {
        lifeText = GetComponent<Text>();
        UpdateDisplay();
    }

    public Status LoseLife(int amount)
    {
        if (lives >= amount)
        {
            lives -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        } else
        {
            return Status.FAILURE;
        }
    }

    private void UpdateDisplay()
    {
        lifeText.text = lives.ToString();
    }
}
