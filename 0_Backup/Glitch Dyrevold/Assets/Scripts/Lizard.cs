﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attacker))]
public class Lizard : MonoBehaviour
{

    private Attacker attacker;
    private Animator animator;

    void Start()
    {
        attacker = GetComponent<Attacker>();
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collidingObject = collision.gameObject;

        if (collidingObject.GetComponent<Fox>())
        {
            Debug.Log("Der var reven ja!");
            return;
        }

        if (!collidingObject.GetComponent<Defender>())
        {
            return;
        }

        animator.SetBool("isAttacking", true);
        attacker.Attack(collidingObject);
    }
}

