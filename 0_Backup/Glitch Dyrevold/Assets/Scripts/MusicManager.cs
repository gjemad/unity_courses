﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] levelMusicChangeArray;

    private AudioSource audioSource;


    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Debug.Log("Didn't destroy '" + name + "' on load!");
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Debug.Log(PlayerPrefsManager.GetMasterVolume());
    }

    #region OnSceneLoaded
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        Debug.Log("Delegation started.");
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        Debug.Log("Delegation over.");
    }

    void OnSceneLoaded(Scene level, LoadSceneMode mode)
    {
        int levelNumber = SceneManager.GetActiveScene().buildIndex;
        AudioClip thisLevelMusic = levelMusicChangeArray[levelNumber];

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            return;
        } else if (thisLevelMusic)
        {
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.volume = PlayerPrefsManager.GetMasterVolume();
            audioSource.Play();
            Debug.Log("Playing clip: " + thisLevelMusic);
        }
    }
    #endregion

    public void ChangeVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
