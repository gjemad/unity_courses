﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

    public float levelTime;

    private Slider slider;
    private LevelManager lvlManager;
    private AudioSource audioSource;
    private Text handleText;
    private bool isEndOfLevel = false;

    void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsManager.GetMasterVolume();
        handleText = GetComponentInChildren<Text>();
        lvlManager = GameObject.FindObjectOfType<LevelManager>();
        slider = GetComponent<Slider>();
        slider.maxValue = levelTime;
        slider.minValue = 0f;
	}

	void Update () {
        levelTime -= Time.deltaTime;
        levelTime = Mathf.Clamp(levelTime, 0f, levelTime);
        slider.value = levelTime;

        float roundedTime = Mathf.Round(levelTime);
        handleText.text = roundedTime.ToString();

        bool isTimeUp = (levelTime <= 0);

        if (isTimeUp && !isEndOfLevel)
        {
            audioSource.Play();
            Invoke("WinLevel", audioSource.clip.length);
            isEndOfLevel = true;
        }
	}

    void WinLevel ()
    {
        lvlManager.LoadLevel("03a_win"); 
    }
}
