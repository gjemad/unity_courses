﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {

    public float SplashDelay;
    public float SplashVolume;
    public AudioClip SplashClip;

    private LevelManager lvlmgr;


    void Start () {
        // Starter coroutine for delay før scenebytte (basert på verdien satt i SplashDelay i Unity
        lvlmgr = GameObject.FindObjectOfType<LevelManager>();
        StartCoroutine(LoadSceneAfterDelay(SplashDelay));
        // Splash Screen lyd
        this.GetComponent<AudioSource>().volume = SplashVolume;
        this.GetComponent<AudioSource>().clip = SplashClip;
        this.GetComponent<AudioSource>().loop = false;
        this.GetComponent<AudioSource>().Play();
    }

    IEnumerator LoadSceneAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        lvlmgr.LoadNextLevel();
    }
}
