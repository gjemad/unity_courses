﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarDisplay : MonoBehaviour {

    private int stars = 9999;
    public enum Status { SUCCESS, FAILURE }

    private Text scoreText;

    void Start () {
        scoreText = GetComponent<Text>();
        UpdateDisplay();
    }

    public void AddStars (int amount)
    {
        stars += amount;
        UpdateDisplay();
    }

    public Status UseStars (int amount)
    {
        if (stars >= amount)
        {
            stars -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        }
        return Status.FAILURE;
    }

    private void UpdateDisplay ()
    {
        scoreText.text = stars.ToString();
    }
}
