﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Kjører ikke kode neden med mindre gameObject har en 'Attacker' component på seg
[RequireComponent (typeof (Attacker))]
public class Fox : MonoBehaviour {

    private Attacker attacker;
    private Animator animator;

	void Start () {
        attacker = GetComponent<Attacker>();
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collidingObject = collision.gameObject;

        if (!collidingObject.GetComponent<Defender>())
        {
            return;
        }

        if (collidingObject.GetComponent<Stone>()) {
            animator.SetTrigger("Jump");
        } else
        {
            animator.SetBool("isAttacking", true);
            attacker.Attack(collidingObject);
        }
    }
}
