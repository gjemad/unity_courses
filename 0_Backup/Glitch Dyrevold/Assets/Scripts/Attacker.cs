﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {

    [Tooltip ("Hvor ofte (i sekunder) skal denne fienden spawne?")]
    public float seenEverySeconds;

    private float currentSpeed;
    private GameObject currentTarget;

	void Start () {
        Rigidbody2D myRigidbody = gameObject.AddComponent<Rigidbody2D>();
        myRigidbody.isKinematic = true;
	}

	void Update () {
        transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);

    }

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    } 
    // Metoden nedenfor brukes kun i animasjonen til objektet, skal kalles på når et angrep utføres slik at skade kan bli gjort til rett tid
    public void AttackCurrentTarget(float damage)
    {
        if (currentTarget)
        {
            Health health = currentTarget.GetComponent<Health>();
            if (health)
            {
                health.DealDamage(damage);
            }
        }
    }

    public void Attack(GameObject obj)
    {
        currentTarget = obj;

    }
}
