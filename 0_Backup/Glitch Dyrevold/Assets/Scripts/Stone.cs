﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent (typeof(Animator))]
public class Stone : MonoBehaviour
{
    private Animator animator;
    private Attacker attacker;

    void Start()
    {
        animator = GetComponent<Animator>();        
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Attacker>())
        {
            animator.SetTrigger("isAttacked");
        }
    }
}
