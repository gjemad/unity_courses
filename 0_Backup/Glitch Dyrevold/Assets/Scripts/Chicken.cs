﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : MonoBehaviour
{

    private Defender defender;
    private Animator animator;

    void Start()
    {
        defender = GetComponent<Defender>();
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject collidingObject = collision.gameObject;

        if (!collidingObject.GetComponent<Attacker>())
        {
            return;
        }

        animator.SetBool("isAttacking", true);
        defender.Attack(collidingObject);
    }
}
