﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSceneManager : MonoBehaviour {

    private LevelManager levelManager;

	void Start () {
        levelManager = GameObject.FindObjectOfType<LevelManager>();
	}

    public void LoadStartLevel()
    {
        levelManager.LoadLevel("02_level-01");
    }
}
