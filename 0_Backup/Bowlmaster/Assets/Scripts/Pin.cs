﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour {

    public float standingTreshold = 3f;
    public float distToRaise = 40f;

    private Rigidbody rigidBody;


    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    public bool IsStanding ()
    {
        Vector3 rotInEuler = transform.rotation.eulerAngles;

        float tiltInX = Mathf.Abs(270 - rotInEuler.x);
        float tiltInZ = Mathf.Abs(rotInEuler.z);

        if(tiltInX < standingTreshold && tiltInZ < standingTreshold)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void RaiseIfStanding ()
    {
        if (IsStanding())
            {
                rigidBody.useGravity = false;
                transform.Translate(new Vector3(0, distToRaise, 0), Space.World);
            } 
    }

    public void Lower()
    { 
        transform.Translate(new Vector3(0, -distToRaise, 0), Space.World);
        rigidBody.useGravity = false;
    }
}
