﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shredder : MonoBehaviour {

    void OnTriggerExit(Collider collider)
    {
        GameObject leavingObject = collider.gameObject;

        if (leavingObject.GetComponent<Pin>())
        {
            Destroy(leavingObject);
        }
    }
}
