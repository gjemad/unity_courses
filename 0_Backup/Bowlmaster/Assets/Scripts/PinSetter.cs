﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinSetter : MonoBehaviour {

    public int lastStandingCount = -1;
    public Text standingDisplay;
    public GameObject pinSet;

    private Animator animator;
    private Ball ball;
    private float lastChangeTime;
    private bool ballEnteredBox;


    void Start()
    {
        animator = GetComponent<Animator>();
        ball = GameObject.FindObjectOfType<Ball>();
        standingDisplay = GameObject.Find("Pins Text").GetComponent<Text>();
    }

    void Update()
    {
        standingDisplay.text = CountStading().ToString();

        if (ballEnteredBox)
        {
            UpdateStandingCountAndSettle();
        }
    }

    public void RaisePins ()
    {
        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            pin.RaiseIfStanding();
        }
    }

    public void LowerPins ()
    {
        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            pin.Lower();
        }
    }

    public void RenewPins()
    {
        Instantiate(pinSet, new Vector3(0f, 15f, 1829f), Quaternion.identity);
        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            pin.GetComponent<Rigidbody>().velocity = Vector3.zero;
            pin.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    void UpdateStandingCountAndSettle()
    {
        int currentStanding = CountStading();

        if (currentStanding != lastStandingCount)
        {
            lastChangeTime = Time.time;
            lastStandingCount = currentStanding;
            return;
        }

        float settleTime = 3f; // Hvor lang tid (i sekunder) det skal ta får pin bli sett på som stående

        if ((Time.time - lastChangeTime) > settleTime)
        {
            PinsHaveSettled();
        }
    }

    void PinsHaveSettled()
    {
        animator.SetTrigger("Tidy");
        ball.Reset();
        lastStandingCount = -1; // Indicates pins have settled, and ball not back in box
        ballEnteredBox = false;
        standingDisplay.color = Color.green;
    }

    int CountStading ()
    {
        int standing = 0;

        foreach (Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            if (pin.IsStanding())
            {
                standing++;
            }
        }

        return standing;
    }

    void OnTriggerEnter(Collider collider)
    {
        GameObject collidingObject = collider.gameObject;

        if (collidingObject.GetComponent<Ball>())
        {
            ballEnteredBox = true;
            standingDisplay.color = Color.red;
        }
    }
}
