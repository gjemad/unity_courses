﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Ball))]
public class BallDragLaunch : MonoBehaviour {

    private Ball ball;
    private Canvas ui;
    private Vector3 startDrag, endDrag;
    private GameObject ui_arrows;

    private float startTime, endTime;


	void Start () {
        ui_arrows = GameObject.Find("Arrows");
        ball = GetComponent<Ball>();
	}

	void Update () {
		
	}

    public void DragStart ()
    {
        startDrag = Input.mousePosition;
        startTime = Time.time;
    }

    public void DragEnd()
    {
        ui_arrows.SetActive(false);

        endDrag = Input.mousePosition;
        endTime = Time.time;

        float dragDuration = endTime - startTime;

        float launchSpeedX = (endDrag.x - startDrag.x) / dragDuration;
        float launchSpeedZ = (endDrag.y - startDrag.y) / dragDuration;

        Vector3 launchVelocity = new Vector3(launchSpeedX, 0, launchSpeedZ);

        ball.Launch(launchVelocity);
    }

    public void MoveStart (float amount)
    {
        if (!ball.inPlay) {
            // Henter nåværende posisjon til ballen (xyz) og legger det i variabelen ballPos (som Vector 3)
            Vector3 ballPos = transform.position;
            // Tar verdien i X-aksen og legger til verdien i xNudge
            float ballXPos = ballPos.x + amount;
            // Setter en begrensning på hvor høyt og lavt ballPos.x verdien kan gå
            ballPos.x = (Mathf.Clamp(ballXPos, -40f, 40f));
            // Endrer ballens posisjon basert på endringene utført ovenfor
            ball.transform.position = ballPos;
        }
    }
}
