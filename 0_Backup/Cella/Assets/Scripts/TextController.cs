﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

    public Text DialogText;

    private enum States {cell_room, toilet, lockpick, door_0, cell_room_final, freedom };
    private States myState;

    bool hasKey = false;

	// Use this for initialization
	void Start () {
        myState = States.cell_room;
	}
	
	// Update is called once per frame
	void Update () {
        print(myState);
        if (myState == States.cell_room) { state_cell_room(); }
        else if (myState == States.door_0) { state_door_0(); }
        else if (myState == States.lockpick) { state_lockpick(); }
        else if (myState == States.toilet) { state_toilet(); }
        else if (myState == States.cell_room_final) { state_cell_room_final(); }
        else if (myState == States.freedom) { state_freedom(); }
    }
    #region State methods
    void state_cell_room()
    {
        DialogText.text = "Du våkner opp i en kald og nedslitt fengselscelle, hvordan havnet du her? Hvordan kommer du deg ut?\n" +
                          "Du ser celledøren og et godt brukt toalett.\n\n" +
                          "Trykk på (C) for å inspisere celledøren eller (T) for å inspisere toalettet.";
        if (Input.GetKeyDown(KeyCode.C))        { myState = States.door_0; }
        else if (Input.GetKeyDown(KeyCode.T))   { myState = States.toilet; }
    }
    void state_door_0()
    {
        if (hasKey == true)
        {
            DialogText.text = "Celledøren er låst. Skulle kanskje sjekket dette før jeg valgte å grave i toalettet.\n" +
                              "Forhåpentligvis så fungerer denne nøkkelen på døren.\n\n" +
                              "Trykk på (N) for å ta i bruk nøkkelen eller (C) for å gå tilbake til cellen.";
            if (Input.GetKeyDown(KeyCode.N))        { myState = States.freedom; }
            else if (Input.GetKeyDown(KeyCode.C))   { myState = States.cell_room; }

        }
        else if (hasKey == false)
        {
            DialogText.text = "Celledøren er låst. Kanskje det ligger noe rundt her du kunne brukt for å åpne døren?\n\n" +
                              "Trykk på (C) for å gå tilbake til cellen.";
            if (Input.GetKeyDown(KeyCode.C))        { myState = States.cell_room; }
        }
    }
    void state_lockpick()
    {
        DialogText.text = "I bunn av toalettskålen ligger det faktisk en nøkkel, kanskje den kan brukes på celledøren?\n\n" +
                          "Trykk på (N) for å plukke opp nøkkelen eller trykk på (C) for å gå tilbake til cellen.";
            if (Input.GetKeyDown(KeyCode.N)) { hasKey = true; myState = States.cell_room_final; }
            if (Input.GetKeyDown(KeyCode.C)) { myState = States.cell_room; }
    }
    void state_toilet()
    {
        if (hasKey == true)
        {
            DialogText.text = "Du står igjen over denne toalettskålen, hvorfor?\n\n" +
                              "Trykk på (N) for å legge fra deg nøkkelen eller (C) for å gå tilbake til cellen.";
            if (Input.GetKeyDown(KeyCode.N))        { hasKey = false; myState = States.cell_room; }
            else if (Input.GetKeyDown(KeyCode.C))   { myState = States.cell_room; }
        }   
        else if (hasKey == false)
        {
            DialogText.text = "Du merker med engang at dette toalettet har vært godt brukt, som at lukten ikke ga den\n" +
                              "beskjeden godt nok.\n" +
                              "Nede i toalettskålen ser du noe som skiller seg ut, ønsker du å ta en titt?\n\n" +
                              "Trykk på (T) for å \"inspisere\" toalettet ytterligere eller (C) for å gå tilbake til cellen.";
            if (Input.GetKeyDown(KeyCode.T))        { myState = States.lockpick; }
            else if (Input.GetKeyDown(KeyCode.C))   { myState = States.cell_room; }
        }
    }
    void state_cell_room_final()
    {
        DialogText.text = "Du står nå midt i cellen, men denne gang med en nøkkel!\n" +
                          "Du ser celledøren og et godt brukt toalett.\n\n" +
                          "Trykk på (C) for å inspisere celledøren eller (T) for å inspisere toalettet.";
        if (Input.GetKeyDown(KeyCode.C)) { myState = States.door_0; }
        else if (Input.GetKeyDown(KeyCode.T)) { myState = States.toilet; }
    }
    void state_freedom ()
    {
        DialogText.text = "Du låser opp celledøren og kan med dette kjenne lukten av frihet...\n\n" +
                          "... great success!!\n\n" +
                          "Trykk på (ENTER) for å spille på nytt.";
        if (Input.GetKeyDown(KeyCode.Return)) { hasKey = false; myState = States.cell_room; }
    }
    #endregion
}
