﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float health = 250f;
    public float speed = 15.0f;
    public float projectileSpeed = 15f;
    public float fireRate = 0.2f;
    public float SfxVolumeProjectile = 0.3f;
    public float SfxVolumeDeath = 1f;
    public float padding = 1f;
    public GameObject projectile;
    public AudioClip shootSfx;
    public AudioClip deathSfx;

    float xMin;
    float xMax;


    void Start()
    {
        // Henter distansen mellom gameobject og kamera (Z-posisjonen)
        float distance = transform.position.z - Camera.main.transform.position.z;
        // Definerer hva som er minimum og maksimum verdier på X-posisjonen (verdier konverteres fra ViewPort til WorldPoint)
        Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance));
        Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));
        // Legger til padding til xMin og xMax, slik at spilleren ikke "clipper" utenfor skjermen
        xMin = leftMost.x + padding;
        xMax = rightMost.x - padding;
        // Spesifiserer hvilken komponent som skal hentes ut av GameObjectet som dette skriptet et tilkoblet
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }

        // Her settes begrensningene på X-posisjonen til spilleren, slik at man ikke kan gå utenfor spilleområdet
        float newX = Mathf.Clamp(transform.position.x, xMin, xMax);
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.00001f, fireRate);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
        }
    }

    void Fire()
    {
        // Setter en offset på hvor prosjektilet skal spawne, 0.8f world units unna midten av GameObject (som også vil si, rett under spilleren)
        Vector3 startPosition = (transform.position + new Vector3(0, .8f, 0));
        // Instantiate spawner et Object ikke et GameObject, vi gjør derfor følgende for å konvertere objektet til et GameObject:
        GameObject beam = Instantiate(projectile, startPosition, Quaternion.identity) as GameObject;
        // Nå har vi muligheten til å hente ut komponenter fra objektet, koden nedenfor skyter avgårde prosjektilet
        beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
        AudioSource.PlayClipAtPoint(shootSfx, transform.position, SfxVolumeProjectile);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();
        Debug.Log(missile);
        if (missile)
        {
            Debug.Log("I got hit!");
            health -= missile.GetDamage();
            missile.Hit();
            if (health <= 0)
            {
                Die();
            }
        }
    }

    void Die ()
    {
        AudioSource.PlayClipAtPoint(deathSfx, transform.position, SfxVolumeDeath);
        LevelManager levelMan = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        levelMan.LoadLevel("End Menu");
        Destroy(gameObject);
    }
}
