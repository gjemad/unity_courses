﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public GameObject projectile;
    public float projectileSpeed = 10f;
    public float health = 150f;
    public float shotsPerSecond = 0.5f;
    public int scoreValue = 150;
    public float SfxVolumeProjectile = 0.3f;
    public float SfxVolumeDeath = 1f;
    public AudioClip shootSfx;
    public AudioClip deathSfx;

    private ScoreKeeper scoreKeeper;


    void Start()
    {
        scoreKeeper = GameObject.Find("ScoreText").GetComponent<ScoreKeeper>();
    }

    void Update()
    {
        float probability = Time.deltaTime * shotsPerSecond;
        if(Random.Range (0f,1f) < probability)
        {
            Fire();
        }
    }

    void Fire()
    {
        // Setter en offset på hvor prosjektilet skal spawne, 0.8f world units unna midten av GameObject (som også vil si, rett under fiende objektet)
        Vector3 startPosition = (transform.position + new Vector3(0, -.6f, 0));
        // Instantiate spawner et Object ikke et GameObject, vi gjør derfor følgende for å konvertere objektet til et GameObject:
        GameObject beam = Instantiate(projectile, startPosition, Quaternion.identity) as GameObject;
        // Nå har vi muligheten til å hente ut komponenter fra objektet, koden nedenfor skyter avgårde prosjektilet
        beam.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
        AudioSource.PlayClipAtPoint(shootSfx, transform.position, SfxVolumeProjectile);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Projectile missile = collider.gameObject.GetComponent<Projectile>();

        if (missile)
        {
            health -= missile.GetDamage();
            missile.Hit();
            scoreKeeper.Score(scoreValue);
            if (health <= 0)
            {
                AudioSource.PlayClipAtPoint(deathSfx, transform.position, SfxVolumeDeath);
                Destroy(gameObject);
            }
        }
    }
}
