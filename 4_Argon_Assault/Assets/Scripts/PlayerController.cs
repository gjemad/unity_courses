﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [SerializeField] float controlSpeed = 65f;
    [SerializeField] float clampX = 45f;
    [SerializeField] float clampY = 35f;
    [SerializeField] GameObject[] guns;

    [Header("Screen-position Based")]
    [SerializeField] float positionPitchFactor = -.37f;
    [SerializeField] float positionYawFactor = .44f;
    [SerializeField] float positionRollFactor = 0f;

    [Header("Control-throw Based")]
    [SerializeField] float controlPitchThrow = -10f;
    [SerializeField] float controlYawThrow = -5f;
    [SerializeField] float controlRollThrow = -10f;

    float xThrow, yThrow;
    bool isPlayerAlive = true;

    void OnPlayerDeath (bool status)
    {
        isPlayerAlive = false;
    }

	void Update ()
    {
        if (isPlayerAlive)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    void ProcessRotation()
    {
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToControlThrow = yThrow * controlPitchThrow;
        float pitch = pitchDueToPosition + pitchDueToControlThrow;

        float yawDueToPosition = transform.localPosition.x * positionYawFactor;
        float yawDueToControlThrow = xThrow * controlYawThrow;
        float yaw = yawDueToPosition + yawDueToControlThrow;

        float rollDueToControlThrow = xThrow * controlRollThrow;
        float roll = transform.localPosition.x * positionRollFactor + rollDueToControlThrow;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = xThrow * controlSpeed * Time.deltaTime;
        yThrow = CrossPlatformInputManager.GetAxis("Vertical");
        float yOffset =  yThrow * controlSpeed * Time.deltaTime;

        float rawNewXPos = transform.localPosition.x + xOffset;
        float rawNewYPos = transform.localPosition.y + yOffset;

        //Debug.Log(string.Format("Throw: {0}\nOffset: {1}", xThrow, xOffset));

        transform.localPosition = new Vector3(Mathf.Clamp(rawNewXPos, -clampX, clampX), Mathf.Clamp(rawNewYPos, -clampY, clampY - 15), transform.localPosition.z);
    }

    void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            ActivateGuns();
        }
        else
        {
            DisableGuns();
        }
    }

    void ActivateGuns()
    {
        foreach (GameObject go in guns)
        {
            go.SetActive(true);
        }
    }

    void DisableGuns()
    {
        foreach (GameObject go in guns)
        {
            go.SetActive(false);
        }
    }
}
