﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] int hits = 10;
    [SerializeField] int scorePoints;
    [SerializeField] Transform parent;

    public GameObject explosionEffect;

    ScoreBoard scoreBoard;

	void Start ()
    {
        AddBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
    }

    void AddBoxCollider()
    {
        Collider enemyCollider = gameObject.AddComponent<BoxCollider>();
        enemyCollider.isTrigger = false;
    }

    void OnParticleCollision(GameObject other)
    {
        ProcessHit();
        if (hits <= 0)
        {
            KillEnemy();
        }
    }

    void ProcessHit()
    {
        scoreBoard.ScoreHit(scorePoints);
        hits--;
    }

    void KillEnemy()
    {
        GameObject go = Instantiate(explosionEffect, transform.position, Quaternion.identity);
        go.transform.parent = parent;
        Destroy(gameObject);
    }
}
