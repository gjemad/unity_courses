﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

    [SerializeField] int scoreAmplifier;

    int score;
    Text scoreText;

    void Start ()
    {
        scoreText = GetComponent<Text>();
        UpdateScore();
	}

    void UpdateScore ()
    {
        scoreText.text = score.ToString();
    }

    public void ScoreHit(int scorePoints)
    {
        score = (score + scorePoints) * scoreAmplifier;
        UpdateScore();
    }
}
