﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

    public static Manager instance = null; // Eksponerer Manager klassen (Singleton-pattern, se Awake)

    [Range(0f, 10f)] public float splashDelay = 3f;
    [Tooltip("Musikk volum")] [Range(0f, 1f)] [SerializeField] float musicVolume = .20f;
    [Tooltip("Size = Settes til antall scener i build settings\nElement 0 -> = Musikkspor for den respektive scenen")] [SerializeField] AudioClip[] levelMusicTracks; // todo cleanup needed!
    public int currentSceneIndex = 0;

    AudioSource audioSource;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    void Start ()
    {
        PlayMusic();
    }

    void PlayMusic()
    {
        audioSource.Stop();
        audioSource.clip = levelMusicTracks[currentSceneIndex];
        audioSource.volume = musicVolume;
        audioSource.loop = true;
        audioSource.Play();
    }

    #region Level Manager
    public void LoadLevel(int number)
    {
        SceneManager.LoadScene(number);
    }
    public void QuitRequest()
    {
        // Andre ting som kan/må gjøres før spillet avsluttes?
        Application.Quit();
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public IEnumerator LoadSceneAfterDelay(int levelNumber, float delay)
    {
        yield return new WaitForSeconds(delay);
        LoadLevel(levelNumber);
    }
    #endregion

    #region OnSceneLoaded
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        Debug.Log("Delegation started.");
    }
    void OnSceneLoaded(Scene level, LoadSceneMode mode)
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0) // Alt som skjer i scene 0 (splash screen) defineres her
        {
            StartCoroutine(LoadSceneAfterDelay(currentSceneIndex + 1,splashDelay));
        }
        else
        {
            PlayMusic();
        }
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        Debug.Log("Delegation over.");
    }
    #endregion
}
