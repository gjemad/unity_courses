﻿using UnityEngine;

public class CollisionHandler : MonoBehaviour {

    [SerializeField] float levelLoadDelay;

    Transform explosion;

    void Awake()
    {
        explosion = transform.Find("Explosion");
    }

    void OnTriggerEnter(Collider collider)
    {
        StartDeathSequence();
    }

    void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath", true);
        explosion.gameObject.SetActive(true);
        StartCoroutine(Manager.instance.LoadSceneAfterDelay(Manager.instance.currentSceneIndex, levelLoadDelay));
    }
}
