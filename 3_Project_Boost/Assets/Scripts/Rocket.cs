﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    [SerializeField] int thrustSpeed;
    [SerializeField] float rotationSpeed;

    [SerializeField] AudioClip soundEngine;
    [SerializeField] AudioClip soundCrash;
    [SerializeField] AudioClip soundNextLevel;
    [SerializeField] AudioClip soundWin;
    [SerializeField] AudioClip soundThrust;

    [SerializeField] ParticleSystem particleDeath;
    [SerializeField] ParticleSystem particleWin;
    [SerializeField] ParticleSystem particleThrust;

    Rigidbody rigidBody;
    AudioSource audioSource;
    BoxCollider rocketBoxCollider;
    MeshRenderer rocketMesh;

    bool isTransitioning = false;

	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        rocketBoxCollider = GetComponent<BoxCollider>();
        rocketMesh = GetComponentInChildren<MeshRenderer>();
        print(rocketMesh);
	}
	
	void Update ()
    {
        if (!isTransitioning)
        {
            RespondToThrustInput();
            RespondToRotateInput();
        }
        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (isTransitioning) { return; }
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            default:
                StartDeathSequence();
                break;
        }
    }

    private void StartSuccessSequence()
    {
        isTransitioning = true;
        audioSource.PlayOneShot(soundNextLevel);
        particleWin.Play();
        Invoke("LoadNextLevel", soundNextLevel.length + 1f);
    }

    private void StartDeathSequence()
    {
        isTransitioning = true;
        Vector3 explosionRNG = new Vector3(0, 0, (Random.Range(-150f, 150f)));
        rigidBody.AddExplosionForce(100f, explosionRNG, 150f);
        audioSource.PlayOneShot(soundCrash);
        particleThrust.Stop();
        particleDeath.Play();
        Invoke("Death", soundCrash.length + 1f);
    }

    private void LoadNextLevel()
    {
        int nextLevelNumber = SceneManager.GetActiveScene().buildIndex + 1;
        if ((nextLevelNumber != (SceneManager.sceneCountInBuildSettings)))
        {
            SceneManager.LoadScene(nextLevelNumber);
        }
        else
        {
            audioSource.PlayOneShot(soundWin);
            print("YOU WON!");
        }
    }

    private void Death()
    {
        print("Restarting...");
        SceneManager.LoadScene(0);
    }

    private void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();
        }
        else
        {
            particleThrust.Stop();
            audioSource.Stop();
        }
    }

    private void RespondToRotateInput()
    {
        rigidBody.angularVelocity = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, 0, (rotationSpeed * Time.deltaTime)));
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 0, (-rotationSpeed * Time.deltaTime)));
        }
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (rocketBoxCollider.enabled)
            {
                rocketBoxCollider.enabled = false;
            }
            else
            {
                rocketBoxCollider.enabled = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(new Vector3(0, (thrustSpeed * 100 * Time.deltaTime), 0));
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(soundEngine);
        }
        particleThrust.Play();
    }
}
